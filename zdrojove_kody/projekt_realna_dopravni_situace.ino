const int AR = 5;
const int AY = 4;
const int AG = 3;

const int PR = 7;
const int PG = 6;

const int AR2 = 11;
const int AY2 = 12;
const int AG2 = 13;

const int PR2 = 8;
const int PG2 = 9;

const int foto = A0;
const int LAMP = 10;

const int btn = 2;
bool chodci = false;
bool prevChodci = false;

void setup() {
  Serial.begin(9600);

  pinMode(AR, OUTPUT);
  pinMode(AY, OUTPUT);
  pinMode(AG, OUTPUT);

  pinMode(PR, OUTPUT);
  pinMode(PG, OUTPUT);

  pinMode(AR2, OUTPUT);
  pinMode(AY2, OUTPUT);
  pinMode(AG2, OUTPUT);

  pinMode(PR2, OUTPUT);
  pinMode(PG2, OUTPUT);

  pinMode(foto, INPUT);
  pinMode(LAMP, OUTPUT);

  pinMode(btn, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(btn), zmena, FALLING);

  turnCarOn('G');
  turnCarOff('Y');
  turnCarOff('R');

  turnWalkerOn('R');
  turnWalkerOff('G');

  prevChodci = chodci;
}

void loop() {
  checkLamp();
  
  if (chodci != prevChodci) { 
    if (chodci) { 
      delay(100);
      
      turnCarOff('G');
      
      delay(2000);
      
      turnCarOn('Y');
      
      delay(2000);
      
      turnCarOff('Y');
      
      turnCarOn('R');
      
      delay(100);
      
      turnWalkerOff('R');
      
      delay(100);
      
      turnWalkerOn('G');

    } else { 
      
      delay(100);
      
      turnWalkerOff('G');
      
      delay(100);
      
      turnWalkerOn('R');

      delay(2000);
      
      turnCarOn('Y');
      
      delay(2000);
      
      turnCarOff('Y');
      
      delay(100);
      
      turnCarOff('R'); // Vypnout červenou LED
      
      delay(100);
      
      turnCarOn('G'); // Zapnout zelenou LED
      
      delay(100);
    }
    
    prevChodci = chodci; 
  }
}

void zmena() {
  chodci = !chodci;

  if (chodci) {
    Serial.println("Zelenou mají chodci.");
  } else {
    Serial.println("Zelenou mají auta.");
  }
  return;
}

void turnCarOn(char type) {
  switch (type) {
    case 'G':
      digitalWrite(AG, HIGH);
      digitalWrite(AG2, HIGH);
      break;
    case 'Y':
      digitalWrite(AY, HIGH);
      digitalWrite(AY2, HIGH);
      break;
    case 'R':
      digitalWrite(AR, HIGH);
      digitalWrite(AR2, HIGH);
      break;
  }
}

void turnCarOff(char type) {
  switch (type) {
    case 'G':
      digitalWrite(AG, LOW);
      digitalWrite(AG2, LOW);
      break;
    case 'Y':
      digitalWrite(AY, LOW);
      digitalWrite(AY2, LOW);
      break;
    case 'R':
      digitalWrite(AR, LOW);
      digitalWrite(AR2, LOW);
      break;
  }
}

void turnWalkerOn(char type) {
  switch (type) {
    case 'G':
      digitalWrite(PG, HIGH);
      digitalWrite(PG2, HIGH);
      break;
    case 'R':
      digitalWrite(PR, HIGH);
      digitalWrite(PR2, HIGH);
      break;
  }
}

void turnWalkerOff(char type) {
  switch (type) {
    case 'G':
      digitalWrite(PG, LOW);
      digitalWrite(PG2, LOW);
      break;
    case 'R':
      digitalWrite(PR, LOW);
      digitalWrite(PR2, LOW);
      break;
  }
}

void checkLamp() {
  int fotoValue = analogRead(foto);
  Serial.println(fotoValue);
  if (fotoValue < 20) {
    digitalWrite(LAMP, HIGH);
  } else {
    digitalWrite(LAMP, LOW);
  }
  delay(500);
}
