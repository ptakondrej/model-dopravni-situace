# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala | Tvorba mi zabrala okolo 12 hodin. |
| jak se mi to podařilo rozplánovat | Rozplánování šlo celkem dobře, s projektem jsem začal celkem brzy a tak jsem měl na vše dost času. Od posledního projektu to šlo mnohem lépe. |
| návrh designu | https://gitlab.spseplzen.cz/ptakondrej/model-dopravni-situace/-/blob/main/dokumentace/design/Nacrt_designu.png |
| proč jsem zvolil tento design | Přišlo mi to jako jednoduché řešení, při kterém bych neměl mít žádný větší problém. |
| zapojení | https://gitlab.spseplzen.cz/ptakondrej/model-dopravni-situace/-/blob/main/dokumentace/design/Tinkercad-zapojeni.png |
| z jakých součástí se zapojení skládá | breadboard, kabely, rezistory 220Ω, veroboard, mosazné dráty |
| realizace | https://gitlab.spseplzen.cz/ptakondrej/model-dopravni-situace/-/tree/main/dokumentace/fotky/Realizace.jpeg |
| jaký materiál jsem použil a proč | Použil jsem: KARTON na krabici, MOSAZNÝ DRÁT - Měl jsem jich hodně a jsou to dobré vodiče, BRČKA - jejich tvar se dobře hodil na semafor, ČERNÁ IZOLEPA - Zakrytí a upevnění brček na semaforech, SEKUNDOVÉ LEPIDLO - Pomocné přidržení |
| co se mi povedlo | Povedlo se mi dobře vybrat materiál a celkem rychle vytvořit prototyp semaforů. |
| co se mi nepovedlo/příště bych udělal/a jinak | Zkrontroloval bych si dřív odpor rezistorů. |
| zhodnocení celé tvorby | Práce na projektu mě do jisté míry bavila, všechno mi šlo celkem dobře, až na pár detailů s rezistory, ale bylo to celkem zábavné a na celý projekt mám pozitivní názor. |
